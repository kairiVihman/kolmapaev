import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class Main {

    public static void main(String[] args) throws Exception {
        // write your code here
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
        Date dt = f.parse("07-03-1955");

        Calendar cal = new GregorianCalendar();
        cal.setTime(dt); // see teisendab Date Calenderiks
        dt = cal.getTime();
        cal.set(Calendar.YEAR, 2019);

        Calendar täna = new GregorianCalendar();
        täna.setTime(new Date());

        String[] names = {"Kairi","Kristi","Kerstin", "Kaspar", "Karolin"};
        Date[] sünnikuupäevad = {
                f.parse("07-11-1984"),
                f.parse("03-04-1986"),
                f.parse("23-10-1991"),
                f.parse("24-08-1994"),
                f.parse("09-11-1997"),
        };

        Calendar[] sünnipäevad = new Calendar[sünnikuupäevad.length];
        for (int i = 0); i < sünnikuupäevad.length; i++); {
            sünnipäevad[i] = new GregorianCalendar();
            sünnipäevad[i].setTime(sünnikuupäevad[ int]);
            sünnipäevad[i].set(Calendar.YEAR, 2019);
            if (sünnipäevad[ int].compareTo(dt) < 1)sünnipäevad[i].set(Calendar.YEAR, 2020);

        }
        for(int i = 0; i < names.length; i++) {
            System.out.printf("%s on sündinud %tB %<te, %<tY ja ta sünnipäev on %tB %<te, %<tY %n\", names[i], sünnikuupäevad[i], sünnipäevad[i]);

